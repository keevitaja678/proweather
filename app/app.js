var app = angular.module('weatherforproekspert', ['jtt_openweathermap']);
app.controller('weatherController', function($scope, openweathermapFactory, $http) {

	$scope.geoLocations = {};
	// Load local storage
	$scope.geoLocations = JSON.parse(localStorage.getItem("savedData"));
	// check if local storage is empty
	$scope.checkNull = function () {
	if (typeof $scope.geoLocations !== 'undefined' && $scope.geoLocations !== null) {
	}
	else {
		$scope.geoLocations = {};
	}
	};
	$scope.checkNull();
	// Check user geoLocation
	$scope.initialGeoLocation = function () {
		// Using ip-api for location since HTML5 does not work for chrome unless HTTPS is enabled.
	$http.get('http://ip-api.com/json')
		.then(function(coordinates) {
			$scope.geoLocation = coordinates.data.city;
			$scope.addLocation();			
	}, function(response) { // Failed
			$scope.geoLocation = "Tallinn";
	});
	};
	$scope.initialGeoLocation();
	// API key
	var _appid = "dbe7c34475e1abf9d84bd98ad4710c3c";
	$scope.addLocation = function () {
	//	use openweathermapFactory to HTTP request data from openweathermap API
    openweathermapFactory.getWeatherFromCitySearchByName({
        q:$scope.geoLocation,
		units: "metric",
        appid:_appid
    }).then(function(response){
		// Save data to geoLocation
		$scope.geoLocations[$scope.geoLocation] = response.data;
		$scope.geoLocation = "";
		// Save data to local storage
		localStorage.setItem('savedData', JSON.stringify($scope.geoLocations));
    }, function(response) { // Failed
		//Errors
		$scope.geoLocations[$scope.geoLocation] = "Sorry no city was found by this name.";
		$scope.geoLocation = "UnKnown";
	}
	);
	};
	// remove location from list and local storage
	$scope.removeLocation = function(key) {
		delete $scope.geoLocations[key];
		localStorage.setItem('savedData', JSON.stringify($scope.geoLocations));
	};
});