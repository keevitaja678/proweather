# README #

A simple weather app using http://openweathermap.org/ for weather data API and http://ip-api.com/json for user locational data.


[DEMO](http://framework.cythan.com/proweather/)

### What is this repository for? ###

To show what can be done with Angularjs

### How do I get set up? ###

Nothing. All the dependencies are included in the repository.
An internet connection is needed for its use. 

### ToDo ###

Fix error handling for when user input is incorrect and when API gives a connection error.

### Who do I talk to? ###

You can write to me at raul@cythan.com